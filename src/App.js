import React, { Component } from 'react';
import { connect } from 'react-redux'
// import './App.css';

import SocketSwitch from './components/SocketSwitch'
import { socketConnect, socketSend, socketMessage } from "./actions/websocket";

class App extends Component {
  componentDidMount() {
    console.log('App loaded');
    console.log('Dispatching connectSocket()')

    const dispatch = this.props.dispatch;

    const eventTarget = new EventTarget();
    eventTarget.addEventListener('socket_message', (event) => {
      const { detail } = event;
      const { data } = detail.message;

      dispatch(socketMessage(data));
    });

    dispatch(socketConnect(eventTarget))
      .then((socket) => {
        let msg = JSON.stringify({
          event: 'subscribe',
          channel: 'trades',
          symbol: 'tBTCUSD'
        });

        dispatch(socketSend(msg))
      })
  }

  render() {
    return (
      <div className="App">
        <SocketSwitch />
        {this.props.websocketData.map(data => <div>${data}</div>)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const websocketData = state.Websocket.data

  return { websocketData }
}
export default connect(mapStateToProps)(App);
