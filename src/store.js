import rootReducer from './reducers';
import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import { socketMiddleware } from "./middlewares/socketMiddleware";

export default function configureStore(socketService, eventTarget) {
  return createStore(
    rootReducer,
    applyMiddleware(
      thunkMiddleware,
      socketMiddleware(socketService, eventTarget)
    )
  );
}