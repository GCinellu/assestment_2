import { combineReducers } from 'redux'

import Websocket from './websocket'

export default combineReducers({
  Websocket
})
