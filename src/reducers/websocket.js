const initialState = {
  connecting: false,
  connected: false,
  stopped: true,
  data: []
}

export default function Websocket( state = initialState, action) {
  switch (`${action.type}_${action.status}`) {
    case 'SOCKET_CONNECTING':
      console.log('SOCKET_CONNECTING action dispatched to reducer...')
      return Object.assign({}, state, {
        stopped: false,
        connecting: true
      });

    case 'SOCKET_CONNECTED':
      console.log('SOCKET_CONNECTED action dispatched to reducer...')
      return Object.assign({}, state, {
        connecting: false,
        connected: true
      });

    case 'SOCKET_MESSAGE':
      console.log('SOCKET_MESSAGE action dispatched to reducer...')
      const currentData = state.data;

      return Object.assign({}, state, {
        data: [...currentData, action.message]
      });
    default:
      return state;
  }
}