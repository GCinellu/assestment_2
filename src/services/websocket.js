export default class SocketService {
  socket;
  url = 'wss://api.bitfinex.com/ws/2';

  connect(eventTarget) {
    console.log('connect had been called');
    console.log(eventTarget)

    this.socket = new WebSocket(this.url);

    return new Promise((resolve, reject) => {
      this.socket.onmessage = message => {
        const event = new CustomEvent('socket_message', {
          detail: { message }
        });

        eventTarget.dispatchEvent(event);
      };
      this.socket.onopen = () => resolve(this.socket);
      this.socket.onerror = () => reject('Socket is not connected');
    })
  }

  disconnect() {
    console.log('disconnecting socket...');
    return new Promise((resolve) => {
      this.socket.close();
      resolve(this.socket);
    })
  }

  send(message) {
    return new Promise((resolve) => {
      this.socket.send(message);

      resolve(this.socket);
    })
  }

  subscribe() {

  }
}