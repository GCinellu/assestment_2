const socketConnecting = (eventTarget) => {
  console.log('Action socketConnecting has been called...')
  return {
    type: 'SOCKET',
    status: 'CONNECTING',
    eventTarget
  }
};

export const socketConnected = () => {
  console.log('Action socketConnected has been called...');
  return {
    type: 'SOCKET',
    status: 'CONNECTED'
  }
}

export const socketMessage = (message) => {
  console.log('Action socketMessage has been called...');
  return {
    type: 'SOCKET',
    status: 'MESSAGE',
    message
  }
}

export const socketSent = (message) => {
  console.log('Action socketSent has been called...');
  return {
    type: 'SOCKET',
    status: 'SEND',
    message
  }
}

export const socketConnect = (eventTarget) => dispatch => dispatch(socketConnecting(eventTarget));
export const socketSend = message => dispatch => dispatch(socketSent(message));