import { socketConnected } from "../actions/websocket";

export function socketMiddleware(socketService) {
  return ({ dispatch, getState }) => next => action => {
    console.log('socketMiddleware had been invoked');

    const state = getState();
    const socketData = state.Websocket.data;

    if (action.type === 'SOCKET') {
      switch (action.status) {
        case 'CONNECTING':
          if (socketData.connected || socketData.connecting) return;

          next(action);
          return socketService.connect(action.eventTarget)
            .then(() => dispatch(socketConnected()))
            .catch(error => console.error(error));
        case 'SEND':
          socketService.send(action.message);
        default:
          return next(action);
      }
    }

    return next(action);
  }
}